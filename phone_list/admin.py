from django.contrib import admin
from phone_list.models import City, Street, Contact, StreetsInCity

admin.site.register(City)
admin.site.register(Street)
admin.site.register(Contact)
admin.site.register(StreetsInCity)
# Register your models here.
