from django.db import models
from django import forms
from django.forms import ModelForm
from django.conf import settings

class City(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
		return self.name

class Street(models.Model):
	name = models.CharField(max_length=100)

	def __unicode__(self):
		return self.name

class StreetsInCity(models.Model):
	city = models.ForeignKey(City)
	street = models.ForeignKey(Street)

	def __unicode__(self):
		return self.city.name + " " + self.street.name

class Contact(models.Model):
    first_name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=100)
    patronymic = models.CharField(max_length=50, blank=True)
    birthday = models.DateField(blank=True, null=True)
    city = models.ForeignKey(City)
    street = models.ForeignKey(Street)
    phone_num = models.CharField(max_length=11)
    mail = models.EmailField(blank=True)

    def __unicode__(self):
		return self.first_name + " " + self.second_name

class ContactForm(ModelForm):

	birthday = forms.DateField(input_formats=['%d-%m-%Y', '%m-%d-%Y', '%d-%m-%y'], required=False)

	class Meta:
		model = Contact
		fields = '__all__'