from django.shortcuts import get_object_or_404, render, redirect
from django.core.context_processors import csrf
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.core import serializers
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from phone_list.models import Contact, City, Street, StreetsInCity, ContactForm

def index(request):

    paginator = Paginator(Contact.objects.all(), 3) # for example

    page = request.GET.get('page')
    try:
        contacts = paginator.page(page)

    except PageNotAnInteger:
        contacts = paginator.page(1)

    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)

    return render(request, 'phone_list/index.html', {'contacts':contacts, 'list':True})

def detail(request, contact_id):

    contact = get_object_or_404(Contact, pk=contact_id)
    return render(request, 'phone_list/detail.html', {'c': contact, 'add': True})

def cities_list(request):

    cities = serializers.serialize("json", City.objects.all())
    return HttpResponse(cities)

def streets(request, city_id):

    str = StreetsInCity.objects.filter(city=city_id)
    response = []

    for val in str:
        response.append({'id':val.street.id, 'name': val.street.name})

    return HttpResponse(json.dumps(response))

def delete(request, contact_id):

	contact = get_object_or_404(Contact, pk=contact_id)
	contact.delete()
	return redirect('index')

def about(request):
    return render(request, 'phone_list/about.html')

def add(request):
    
    if request.method == 'POST':

    	if 'delete' in request.POST:
    		return redirect('delete', contact_id = request.POST['pk'])

    	if request.POST['pk']:
    		contact = Contact.objects.get(pk=request.POST['pk'])
    	else:
    		contact = Contact()

        form = ContactForm(request.POST, instance=contact)
        
        if form.is_valid():
            form.save()
            return redirect('index')

        return HttpResponse(form.errors)

    else:
        return render(request, 'phone_list/detail.html', {'add': True})
