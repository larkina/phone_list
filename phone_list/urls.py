from django.conf.urls import patterns, url

from phone_list import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^cities_list/$', views.cities_list, name='cities_list'),
    url(r'^streets/(?P<city_id>\d+)/$', views.streets, name='streets'),
    url(r'^(?P<contact_id>\d+)/$', views.detail, name='detail'),
    url(r'^(?P<contact_id>\d+)/delete/$', views.delete, name='delete'),
    url(r'^add/$', views.add, name='add'),
    url(r'^about/$', views.about, name='about'),
)

