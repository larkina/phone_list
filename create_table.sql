BEGIN;
CREATE TABLE "phone_list_city" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL
)
;
CREATE TABLE "phone_list_street" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL
)
;
CREATE TABLE "phone_list_streetsincity" (
    "id" integer NOT NULL PRIMARY KEY,
    "city_id" integer NOT NULL REFERENCES "phone_list_city" ("id"),
    "street_id" integer NOT NULL REFERENCES "phone_list_street" ("id")
)
;
CREATE TABLE "phone_list_contact" (
    "id" integer NOT NULL PRIMARY KEY,
    "first_name" varchar(50) NOT NULL,
    "second_name" varchar(100) NOT NULL,
    "patronymic" varchar(50) NOT NULL,
    "birthday" date NOT NULL,
    "city_id" integer NOT NULL REFERENCES "phone_list_city" ("id"),
    "street_id" integer NOT NULL REFERENCES "phone_list_street" ("id"),
    "phone_num" varchar(11) NOT NULL,
    "mail" varchar(75) NOT NULL
)
;

COMMIT;